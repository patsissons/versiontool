﻿using DigitalHarmoniX.DotNETJPP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.UI;

namespace DigitalHarmoniX.VersionTool
{
    class Program
    {
        public const string DefaultPattern = @"const string (\S+) = ""[^""]*"";";
        public const string DefaultReplacementFormat = @"const string $1 = ""{0}"";";
        public const int DefaultKeyIndex = 1;

        public const string UsageFormat = @"
Usage:
    {0} <properties file> <target file> [pattern] [replacement format] [key index]";

        public static readonly string Usage = string.Format(UsageFormat, AppName);

        static int Main(string[] args)
        {
            var result = 0;

            try
            {

                if (args.Length < 2)
                {
                    Console.WriteLine(Usage);
                }

                var propFile = new FileInfo(args[0]);
                var targFile = new FileInfo(args[1]);

                if (propFile.Exists == false)
                {
                    Console.WriteLine("Properties file not found: {0}", propFile.FullName);

                    result = -1;
                }

                if (targFile.Exists == false)
                {
                    Console.WriteLine("Properties file not found: {0}", targFile.FullName);

                    result = -1;
                }

                var pattern = DefaultPattern;
                var replacementFormat = DefaultReplacementFormat;
                var keyIndex = DefaultKeyIndex;

                if (args.Length >= 3)
                {
                    pattern = args[2];
                }

                if (args.Length >= 4)
                {
                    replacementFormat = args[3];
                }

                if (args.Length >= 5)
                {
                    keyIndex = Convert.ToInt32(args[4]);
                }

                if (result == 0)
                {
                    result = Process(propFile, targFile, pattern, replacementFormat, keyIndex);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e);

                result = -1;
            }

            return result;
        }

        internal static int Process(FileInfo propFile, FileInfo targFile, string pattern, string replacementFormat, int keyIndex)
        {
            var result = 0;

            var properties = Parser.ParseFile(propFile);

            if (properties != null)
            {
                var targetContent = string.Empty;

                using (var fs = targFile.OpenRead())
                using (var sr = new StreamReader(fs))
                {
                    targetContent = sr.ReadToEnd();
                }

                using (var fs = targFile.OpenWrite())
                using (var sw = new StreamWriter(fs))
                {
                    foreach (var line in targetContent.Split(new[] { Environment.NewLine }, StringSplitOptions.None))
                    {
#if DEBUG
                        Console.WriteLine(ProcessLine(line, pattern, replacementFormat, keyIndex, properties));
#else
                        sw.WriteLine(ProcessLine(line, pattern, replacementFormat, properties));
#endif
                    }
                }
            }
            else
            {
                Console.WriteLine("Unable to parse properties file: {0}", propFile.FullName);

                result = -1;
            }

            return result;
        }

        internal static string ProcessLine(string line, string pattern, string replacementFormat, int keyIndex, Dictionary<string, string> properties)
        {
            var match = Regex.Match(line, pattern);

            if (match.Success)
            {
                var key = match.Groups[keyIndex].Value;

                if (properties.ContainsKey(key))
                {
                    line = Regex.Replace(line, pattern, string.Format(replacementFormat, ProcessValue(properties[key])));
                }
            }

            return line;
        }

        internal static string ProcessValue(string input)
        {
            const string ExpressionPattern = @"(\\)?\$((\{([^\}]+)\})|(\()([^\)]+)(\)))";

            var output = input;

            var matches = Regex.Matches(input, ExpressionPattern);

            foreach (Match match in matches)
            {
                // make sure it wasn't escaped
                if (match.Groups[1].Success == false)
                {
                    if (match.Groups[4].Success)
                    {
                        // Property expression
                        output = Regex.Replace(output, ExpressionPattern, ProcessProperty(match.Groups[4].Value));
                    }
                    else if (match.Groups[6].Success)
                    {
                        // command expression
                        output = Regex.Replace(output, ExpressionPattern, ProcessCommand(match.Groups[6].Value));
                    }
                }
            }

            return output;
        }

        #region Property Expressions

        internal static string ProcessProperty(string property)
        {
            const string PropertyPattern = @"^(\w+)\.([\w\.\[\]]+)(,(.*))?$";

            string value = null;

            var match = Regex.Match(property, PropertyPattern);

            if (match.Success)
            {
                switch (match.Groups[1].Value)
                {
                    case "DateTime":
                        value = ProcessDateTimeProperty(match.Groups[2].Value, match.Groups[4].Value);
                        break;
                    default:
                        value = string.Format("Invalid Property: {0}", property);
                        break;
                }
            }
            else
            {
                value = string.Format("Invalid Property: {0}", property);
            }

            return value;
        }

        internal static string ProcessDateTimeProperty(string property, string format)
        {
            const string PropertyPattern = @"^(\w+)\.?(.*)$";

            string value = null;

            var match = Regex.Match(property, PropertyPattern);

            if (match.Success)
            {
                switch (match.Groups[1].Value)
                {
                    case "Now":
                        value = GetDateTimeProperty(DateTime.Now, match.Groups[2].Value, format);
                        break;
                    case "Today":
                        value = GetDateTimeProperty(DateTime.Today, match.Groups[2].Value, format);
                        break;
                    case "UtcNow":
                        value = GetDateTimeProperty(DateTime.UtcNow, match.Groups[2].Value, format);
                        break;
                    default:
                        value = string.Format("Invalid DateTime Property: {0}", property);
                        break;
                }
            }
            else
            {
                value = "No DateTime Property Provided";
            }

            return value;
        }

        internal static string GetDateTimeProperty(DateTime value, string property, string format)
        {
            string output = null;
            
            if (string.IsNullOrEmpty(property))
            {
                output = value.ToString(format);
            }
            else
            {
                try
                {
                    var eval = DataBinder.Eval(value, property);

                    if (eval is IFormattable)
                    {
                        output = ((IFormattable)eval).ToString(format, null);
                    }
                    else
                    {
                        output = eval.ToString();
                    }
                }
                catch
                {
                    output = value.ToString(format);
                }
            }

            return output;
        }

        #endregion

        #region Command Expressions

        internal static string ProcessCommand(string command)
        {
            var value = command;

            return value;
        }

        #endregion

        internal static string AppName { get { return Assembly.GetExecutingAssembly().GetName().Name; } }
    }
}
